

var primek = {

    lista: [],
    idoStart: 0,
    idoStop: 0,
    min: 0,
    max: 0,
    selected: 0,
    
    setStart: function () {
        'use strict';
        var time = new Date();
        this.idoStart = time.getTime();
    },
    
    setStop: function() {
        'use strict';
        var time = new Date();
        this.idoStop = time.getTime();
    },

    setMin: function (szam) {
        'use strict';
        this.min = szam * 1;
    },

    setMax: function (szam) {
        'use strict';
        this.max = szam * 1;
    }
};


function primTest (_, min, max) {
    'use strict';
    primTT[_](min, max);
}

var primTT = [
function(min, max) {
    'use strict';
    primek.setStart();
    for (var i = min; i <= max; i++) {
                if (primTest.prim1(i)) {
                    primek.lista.push(i);
                }
            }
            primek.setStop();
            return primek.lista;
        },
function(min, max) {
    'use strict';
    primek.setStart();
    for (var i = min; i <= max; i++) {
                if (primTest.prim2(i)) {
                    primek.lista.push(i);
                }
            }
            primek.setStop();
            return primek.lista;
    },
function(min, max) {
    'use strict';
    primek.setStart();
    var temp = primTest.prim3(min, max);
    primek.setStop();
    return temp;
},
function(min, max) {
    'use strict';
    primek.setStart();
    for (var i = min; i <= max; i++) {
                if (primTest.prim4(i)) {
                    primek.lista.push(i);
                }
            }
            primek.setStop();
            return primek.lista;
    }
];



//Public prim tesztelők / prime test methods - usage prim1, 2, 3, 4(number)
// Egyszerű megoldás - megszámolja a számok osztóit 
// Simple method - counts divisors
var primTest = {
    prim1: function (szam) {
        'use strict';
        var osztoDB = 0;
        for (var i = 1; i <= szam; i++) {
            if (szam % i === 0) {
                osztoDB++;
            }
        }
        return (osztoDB === 2);
    },
    // Egyszerű ++ - csak a szám győkéig nézi az osztókat 
    // Simple method ++ - counts divisors, but only to square root of the number
    prim2: function (szam) {
        'use strict';
        var osztoDB = 1;
        if (szam === 1) osztoDB++;
        for (var i = 2; i <= Math.sqrt(szam); i++) {
            if (szam % i === 0) {
                osztoDB++;
            }
        }
        return (osztoDB === 1);
    },
    //Eratoszthenész szitája - 
    prim3: function (min, max) {
            'use strict';
        if (min < 2 ) min = 2;
        var list = [];
        for (var i = min; i <= max; i++) {
            list[i] = true;
        }

        for (var j = min; j <= Math.sqrt(max) ; j++) {
            if (list[j] === true) {
                for (var k = (j * j) ; k <= max; k += j) {
                    list[k] = false;
                }
            }
        }
        for (var n = list.length - 1; n >= 0; n--) {
            if (list[i]) primek.lista.push(i);
        }
        return primek.lista;
    },

    prim4: function (szam) {
        'use strict';
        if (szam % 2 === 0 && szam != 2 || szam === 1) {
            return false;
        } else if (szam % 3 === 0 && szam != 3) {
            return false;
        } else if (szam % 5 === 0 && szam != 5) {
            return false;
        } else if (szam % 7 === 0 && szam != 7) {
            return false;
        } else if (szam % 11 === 0 && szam != 11) {
            return false;
        } else if (szam % 13 === 0 && szam != 13) {
            return false;
        } else if (szam >= 13) {
            var osztóDB = 1;
            for (var i = 13; i <= Math.sqrt(szam) ; i += 2) {
                if (osztóDB > 1) return false;
                else if (szam % i === 0) {
                    osztóDB++;
                }
            }
            return (osztóDB === 1);
        } else {
            return true;
        }
    }
};