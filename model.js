//Object Literal primek
var primek = {

    lista: [],
    idoStart: 0,
    idoStop: 0,
    min: 0,
    max: 0,
    
    setStart: function () {
            var time = new Date();
            this.idoStart = time.getTime();
    },
    
    setStop: function() {
        var time = new Date();
        this.idoStop = time.getTime();
    },

    setMin: function (szam) {
        this.min = szam * 1;
    },

    setMax: function (szam) {
        this.max = szam * 1;
    }
};

var runTest = function () {
    var runtime = 0;
    primek.lista = [];
    primek.setStart();
    if (primek.min === 0 || primek.max === 0 || (primek.max - primek.min) < 1) {
        console.log("Please run .init first");
    } else {
        runSelected(primek.min, primek.max);
        primek.setStop();
        runtime = primek.idoStop - primek.idoStart;
        console.log("Runtime: " + runtime + " ms");
    }
    return runtime;
};

function arraytest(tomb) {
    for (var i = 1; i <= tomb.length; i++) {
        primTest.prim2(tomb[i]);
    }
};
function runSelected(min, max) {

    var selected = $("#selector").prop("selectedIndex");

    switch (selected) {
        case 0:
            break;
        case 1:
            for (var i = min; i <= max; i++) {
                if (primTest.prim1(i)) {
                    primek.lista.push(i);
                }
            };
            $("#btnStrat").removeAttr('disabled', 'disabled');
            break;
        case 2:
            for (var i = min; i <= max; i++) {
                if (primTest.prim2(i)) {
                    primek.lista.push(i);
                }
            };
            $("#btnStrat").removeAttr('disabled', 'disabled');
            break;
        case 3:
            for (var i = min; i <= max; i++) {
                if (primTest.prim3(i)) {
                    primek.lista.push(i);
                }
            };
            $("#btnStrat").removeAttr('disabled', 'disabled');
            break;
        case 4:
            for (var i = min; i <= max; i++) {
                if (primTest.prim4(i)) {
                    primek.lista.push(i);
                }
            };
            $("#btnStrat").removeAttr('disabled', 'disabled');
            break;
    }
};

//Public prim tesztelők / prime test methods - usage prim1, 2, 3, 4(number)
// Egyszerű megoldás - megszámolja a számok osztóit 
// Simple method - counts divisors
var primTest = {
    prim1: function (szam) {
        var osztoDB = 0;
        for (var i = 1; i <= szam; i++) {
            if (szam % i === 0) {
                osztoDB++;
            }
        }
        return (osztoDB === 2);
    },
    // Egyszerű ++ - csak a szám győkéig nézi az osztókat 
    // Simple method ++ - counts divisors, but only to square root of the number
    prim2: function (szam) {
        var osztoDB = 1;
        if (szam === 1) osztoDB++;
        for (var i = 2; i <= Math.sqrt(szam); i++) {
            if (szam % i === 0) {
                osztoDB++;
            }
        }
        return (osztoDB === 1);
    },
    //Eratoszthenész szitája - 
    prim3: function (szam) {
        var list = [];
        for (var i = 2; i <= szam; i++) {
            list[i] = true;
        }

        for (var j = 2; j <= Math.sqrt(szam) ; j++) {
            if (list[j] === true) {
                for (var k = (j * j) ; k <= szam; k += j) {
                    list[k] = false;
                }
            }
        }

        return list[szam];
    },

    prim4: function (szam) {
        if (szam % 2 === 0 && szam != 2 || szam === 1) {
            return false;
        } else if (szam % 3 === 0 && szam != 3) {
            return false;
        } else if (szam % 5 === 0 && szam != 5) {
            return false;
        } else if (szam % 7 === 0 && szam != 7) {
            return false;
        } else if (szam % 11 === 0 && szam != 11) {
            return false;
        } else if (szam % 13 === 0 && szam != 13) {
            return false;
        } else if (szam >= 13) {
            var osztóDB = 1;
            for (var i = 13; i <= Math.sqrt(szam) ; i += 2) {
                if (osztóDB > 1) return false;
                else if (szam % i == 0) {
                    osztóDB++;
                }
            }
            return (osztóDB === 1);
        } else {
            return true;
        }
    }
};