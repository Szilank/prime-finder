jQuery(document).ready(function () {
    observer();

    $("#btnStrat").attr('disabled', 'disabled');
});

var StrategiaSelector = function () {
    var selected = $("#selector").prop("selectedIndex");
    $("#stratBody > div").hide();
    switch (selected) {
        case 0:
            break;
            $("#btnStrat").attr('disabled', 'disabled');
        case 1:
            $("#strat1").show();
            break;
        case 2:
            $("#strat2").show();
            break;
        case 3:
            $("#strat3").show();
            break;
        case 4:
            $("#strat4").show();
            break;
        case 5:
            $("#strat5").show();
            break;
    }
};

//function runMultitest() {
//    var cycle = 0;
//    var delay = 0;


//        cycle = 0;
//        primek.setStart();
//        progressBar.reset();
//        progressBar.show();
//        var progress = window.setInterval(progressBar.update(3), 100);
//        var randNums = [];
//        var out = "";
//        //prim1
//        for (var i = 0; i <= 10000; i++) {
//            randNums[i] = Math.floor((Math.random() * primek.max) + primek.min);
//            primTest.prim1(randNums[i]);
//            cycle += 1;
//        }
//        primek.setStop();
//        var time1 = primek.idoStop - primek.idoStart;
//        out += ("Prim1 \n" + "Eltelt idő: " + time1 + " ms \n" +
//        "Futások száma: " + cycle + "\n");

//        //prim2
//        primek.setStart();
//        cycle = 0;
//        for (var i = 1; i <= 10000; i++) {
//            primTest.prim2(randNums[i]);
//            cycle += 1;
//        }
//        primek.setStop();
//        var time2 = primek.idoStop - primek.idoStart;       
//        out += ("\nPrim2 \n" + "Eltelt idő: " + time1 + " ms \n" +
//        "Futások száma: " + cycle + "\n");

//        //prim3
//        primek.setStart();
//        cycle = 0;
//        for (var i = 1; i <= 10000; i++) {
//            primTest.prim1(randNums[i]);
//            cycle += 1;
//        }
//        primek.setStop();
//        var time3 = primek.idoStop - primek.idoStart;
//        out += ("\nPrim3 \n" + "Eltelt idő: " + time1 + " ms \n" +
//         "Futások száma: " + cycle + "\n");

//        //prim4
//        primek.setStart();
//        cycle = 0;
//        for (var i = 1; i <= 10000; i++) {
//            primTest.prim1(randNums[i]);
//            cycle += 1;
//        }
//        primek.setStop();
//        var time4 = primek.idoStop - primek.idoStart;
//        out += ("\nPrim4 \n" + "Eltelt idő: " + time1 + " ms \n" +
//        "Futások száma: " + cycle);
//        progressBar.hide();
//        window.clearInterval(progress);
//        GuiHandler.outputWrite(out);

//};

function runMultitest() {
    var out = "";

    $("#selector").prop('selectedIndex', '1');
    var time1 = runTest();
    out += ("Egyszerű megoldás \n" + "Eltelt idő: " + time1 + " ms \n" + "\n");

    $("#selector").prop('selectedIndex', '2');
    var time2 = runTest();
    out += ("Egyszerű gyökös \n" + "Eltelt idő: " + time2 + " ms \n" + "\n");

    $("#selector").prop('selectedIndex', '3');
    var time3 = runTest();
    out += ("Eratoszthenész szitája \n" + "Eltelt idő: " + time3 + " ms \n" + "\n");

    $("#selector").prop('selectedIndex', '4');
    var time4 = runTest();
    out += ("If-If-Else \n" + "Eltelt idő: " + time4 + " ms \n" + "\n");

    GuiHandler.outputWrite(out);
    $("#selector").prop('selectedIndex', '0');
};

//Observer

var observer = function () {
    //Kezdő értékek átvétele
    primek.setMin($(".input-min").prop('value'));
    primek.setMax($(".input-max").prop('value'));

    //Multitest gomb click figyelő
    $("#multitest").click(function () {  
        runMultitest();
        $("#btnStrat").attr('disabled', 'disabled');
    });

    //Multitest gomb rolover figyelő
    $("#multitest").mouseover(function () {
        $("#multitest").attr('title', 'Többszörös teszt a megadott intervallumban');
        $("#multitest").tooltip('show');
    });

    //Stratégia gomb - popup
    $("#btnStrat").click(function () {
        StrategiaSelector();
    });

    //Selector change figyelő
    $("select").change(function () {
        $("#hiba").hide();
        var strat = $('select').val();

        if (primek.min === "" || primek.max === "" || (primek.max - primek.min) < 1) {
            $("#hiba").show();
        } else {
            runTest();
            var primList = "";
            for (var j = 0; j < primek.lista.length; j++) {
                primList += (primek.lista[j] + ", ");
            }
            $("textarea").val("Eltelt idő: " + (primek.idoStop - primek.idoStart) + " ms \n" +
            "Stratégia: " + strat + "\n" + "Darab: " + primek.lista.length + "\n" + primList);
        }
    });

    //input field figyelők
    $(".input-min").change(function () {
        primek.setMin($(".input-min").prop('value'));
    });

    $(".input-max").change(function () {
        primek.setMax($(".input-max").prop('value'));
    });
    //-vége

};