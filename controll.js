
var GuiHandler = {
    outputWrite: function(someText){
        $("textarea").val(someText);
    },

    outputClear: function () {
        $("textarea").val("");
    }
}

var progressBar = {
    valuenow: 0,

    reset: function () {
        progressBar.valuenow = 0;
        $(".progress-bar").attr('aria-valuenow', '0');
        $(".progress-bar").attr('style', 'width: 0%');
    },

    updateBy: function (tick) {
        progressBar.valuenow += tick;
        if (progressBar.valuenow > 100) { progressBar.valuenow = 100 }
        $(".progress-bar").attr('aria-valuenow', progressBar.valuenow);
        $(".progress-bar").attr('style', 'width: ' + progressBar.valuenow + '%');
    },

    update: function() {
        progressBar.valuenow += 10;
        if (progressBar.valuenow > 100) { progressBar.valuenow = 100 }
        $(".progress-bar").attr('aria-valuenow', progressBar.valuenow);
        $(".progress-bar").attr('style', 'width: ' + progressBar.valuenow + '%');
    },

    show: function () {
        $(".progress").show();
    },

    hide: function () {
        $(".progress").hide();
    }
};

